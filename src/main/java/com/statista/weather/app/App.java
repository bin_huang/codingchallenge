package com.statista.weather.app;

import java.io.IOException;

public class App {
    public static void main(String[] args) throws IOException {
        ReadAndShow inAndOut = new ReadAndShow();
        DarkSkyWeatherService dWService = new DarkSkyWeatherService();

        boolean nextCheck = true;
        while (nextCheck) {
            String location = inAndOut.readInput();
            dWService.run(location);
            nextCheck = inAndOut.checkNext(); // want to make another check?
        }
    }
}
