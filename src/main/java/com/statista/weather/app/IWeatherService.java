package com.statista.weather.app;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.simple.JSONObject;

/**
 * Please add useful methods to this interface to retrieve weather information
 */
public interface IWeatherService {
    URL buildCityURL(String s) throws MalformedURLException;

    URL buildWeatherURL(JSONObject latAndLng) throws MalformedURLException;

    JSONObject getMetaData(URL url) throws IOException;

    void run(String city) throws IOException;

    JSONObject getCoordinates(JSONObject metaData);

    void showResults(JSONObject weatherMetaInfo, String formattedAddress);
}
