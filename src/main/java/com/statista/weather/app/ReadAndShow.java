package com.statista.weather.app;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class ReadAndShow implements InputAndDisplay {

    @Override
    public String readInput() throws UnsupportedEncodingException {
        //receive user input by giving instructions
        Scanner myScanner = new Scanner(System.in);
        System.out.println("Which location's weather do you curious about?");

        return URLEncoder.encode(myScanner.nextLine(), StandardCharsets.UTF_8.toString());
    }

    @Override
    public Boolean checkNext() throws UnsupportedEncodingException {
        boolean nextCheck = true;
        Scanner myScanner = new Scanner(System.in);
        System.out.println(" Type \"no \" to exit or any other word for next check: ");

        String nextInput = URLEncoder.encode(myScanner.nextLine(), StandardCharsets.UTF_8.toString());
        if (nextInput.equalsIgnoreCase("no")) {
            nextCheck = false;
            myScanner.close();
        }
        return nextCheck;
    }
}
