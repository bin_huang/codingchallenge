package com.statista.weather.app;

import java.io.UnsupportedEncodingException;

public interface InputAndDisplay {
    String readInput() throws UnsupportedEncodingException;

    Boolean checkNext() throws UnsupportedEncodingException;
}
