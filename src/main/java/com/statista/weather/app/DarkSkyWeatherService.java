package com.statista.weather.app;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class DarkSkyWeatherService implements IWeatherService {
    final String geoPath = "https://maps.googleapis.com/maps/api/geocode/json?address=";
    final String geoAPI = "&key=AIzaSyA_-F7_JvTVbY26z46rpABfd163OZuJS2c";

    final String weatherPath = "https://api.darksky.net/forecast/ddfc1b9b6f77699753e558f319bbeafa/";
    final String excludeInfo = "?exclude=[minutely,hourly,daily,alerts,flags]";


    @Override
    // create the URL query for checking city coordinates
    public URL buildCityURL(String city) throws MalformedURLException {
        return new URL(geoPath + city + geoAPI);
    }

    @Override
    // create the URL query for checking the weather information
    public URL buildWeatherURL(JSONObject latAndLng) throws MalformedURLException {
        Double lat = (Double) latAndLng.get("lat");
        Double lng = (Double) latAndLng.get("lng");
        return new URL(weatherPath + lat.toString() + ',' + lng.toString() + excludeInfo);

    }

    @Override
    // get the metadata after sending url get request
    public JSONObject getMetaData(URL url) throws IOException {
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.connect();

        int responseCode = con.getResponseCode();
        if (responseCode != 200) {
            throw new RuntimeException("HttpResponseCode: " + responseCode);
        } else {
            StringBuilder infoString = new StringBuilder();
            Scanner scanner = new Scanner(url.openStream());

            while (scanner.hasNext()) {
                infoString.append(scanner.nextLine());
            }
            scanner.close();

            JSONParser parser = new JSONParser();
            try {
                return (JSONObject) parser.parse(String.valueOf(infoString));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return null;
    }


    @Override
    // get location coordinates
    public JSONObject getCoordinates(JSONObject cityInformation) {
        JSONArray cityInfo = (JSONArray) cityInformation.get("results");
        JSONObject locationInformation = (JSONObject) cityInfo.get(0);

        JSONObject geoInformation = (JSONObject) locationInformation.get("geometry");
        String formattedAddress = (String) locationInformation.get("formatted_address");
        JSONObject corrAndAddress = (JSONObject) geoInformation.get("location");
        corrAndAddress.put("formatted_address", formattedAddress);
        return corrAndAddress;
    }

    @Override
    // show location's weather
    public void showResults(JSONObject weatherMetaInfo, String formattedAddress) {
        JSONObject currInformation = (JSONObject) weatherMetaInfo.get("currently");
        Double currHumidity = (Double) currInformation.get("humidity");
        Double fahrenheitTemp = (Double) (currInformation.get("temperature"));
        String summary = (String) currInformation.get("summary");
        System.out.println("Current weather conditions of location " + formattedAddress + ":");
        System.out.println("****************************************");
        System.out.println("Humidity:" + currHumidity + "%rh");
        System.out.println("****************************************");
        System.out.println("Temperature: " + Math.round((fahrenheitTemp - 32) * 0.5556) + "°C");
        System.out.println("****************************************");
        System.out.println("Short summary:" + summary);
        System.out.println("****************************************");

    }

    @Override
    // the function body
    public void run(String city) throws IOException {
        URL cityURL = buildCityURL(city);
        JSONObject cityInformation = getMetaData(cityURL);

        if (cityInformation == null) {
            System.out.println("Query doesn't work now, check again later.");
        } else {
            String status = (String) cityInformation.get("status");
            Scanner myScanner = new Scanner(System.in);
            while (status.equals("ZERO_RESULTS")) {
                System.out.println("The given location may not exist, please re-enter a new location: ");

                city = URLEncoder.encode(myScanner.nextLine(), StandardCharsets.UTF_8.toString());
                cityInformation = getMetaData(buildCityURL(city));
                status = (String) cityInformation.get("status");
            }

            // get coordinates and formatted address
            JSONObject corrAndAddress = getCoordinates(cityInformation);

            // get weather information
            URL weatherURL = buildWeatherURL(corrAndAddress);
            JSONObject weatherMetaData = getMetaData(weatherURL);
            String formattedAddress = (String) corrAndAddress.get("formatted_address");
            showResults(weatherMetaData, formattedAddress);
        }
    }
}
